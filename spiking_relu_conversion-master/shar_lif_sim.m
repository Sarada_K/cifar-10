function cnn = shar_lif_sim(cnn, test_rt,test_gt,test_bt, test_ymt, opts)
num_examples = size(test_rt, 3);
num_classes  = size(test_ymt, 1);
% Initialize a neuron-based network - needs to be activated to get all the
%   sizes. Shouldn't be an issue after training, unless cleaned.
layer_details = [32,28,14,10,5,2,1];
output_details = [3,64,64,64,64,64,64];
for l = 1 : numel(cnn.layers)
    outputmaps = output_details(l); %numel(cnn.layers{l}.a);
    cnn.layers{l}.mem = cell(1, outputmaps);
    for j=1:outputmaps
        correctly_sized_zeros = zeros(layer_details(l),layer_details(l),1000); %zeros(size(cnn.layers{l}.a{j}, 1), ...
            %size(cnn.layers{l}.a{j}, 2), num_examples);
        cnn.layers{l}.mem{j} = correctly_sized_zeros;
        cnn.layers{l}.refrac_end{j} = correctly_sized_zeros;        
        cnn.layers{l}.sum_spikes{j} = correctly_sized_zeros;
    end    
end
cnn.sum_fv1 = zeros(size(cnn.ffW1,2), num_examples);
cnn.o1_mem        = zeros(64, num_examples);
cnn.o1_refrac_end = zeros(64, num_examples);
cnn.o1_sum_spikes = zeros(64, num_examples);

cnn.sum_fv2 = zeros(size(cnn.ffW2',2), num_examples);
cnn.o2_mem        = zeros(num_classes, num_examples);
cnn.o2_refrac_end = zeros(num_classes, num_examples);
cnn.o2_sum_spikes = zeros(num_classes, num_examples);

cnn.performance  = [];
% Precache answers
[~, ans_idx] = max(test_ymt);

    rescale_fac = 1/(opts.dt*opts.max_rate);
    spike_snapshot = rand(size(test_rt)) * rescale_fac;
    inp_image_r = test_rt; %spike_snapshot <= test_rt;
    inp_image_b = test_bt; %spike_snapshot <= test_bt;
    inp_image_g = test_gt; %spike_snapshot <= test_gt;

    cnn.layers{1}.spikes{1} = inp_image_r;
    cnn.layers{1}.spikes{2} = inp_image_g;
    cnn.layers{1}.spikes{3} = inp_image_b;
%     


for t = 0:opts.dt:opts.duration
    % Create poisson distributed spikes from the input images
    %   (for all images in parallel)
%     rescale_fac = 1/(opts.dt*opts.max_rate);
%     spike_snapshot = rand(size(test_rt)) * rescale_fac;
%     inp_image_r = test_rt; %spike_snapshot <= test_rt;
%     inp_image_b = test_bt; %spike_snapshot <= test_bt;
%     inp_image_g = test_gt; %spike_snapshot <= test_gt;
% 
    cnn.layers{1}.spikes{1} = inp_image_r;
    cnn.layers{1}.spikes{2} = inp_image_g;
    cnn.layers{1}.spikes{3} = inp_image_b;
% %     
    cnn.layers{1}.mem{1} = cnn.layers{1}.mem{1} + inp_image_r;
    cnn.layers{1}.mem{2} = cnn.layers{1}.mem{2} + inp_image_g;
    cnn.layers{1}.mem{3} = cnn.layers{1}.mem{3} + inp_image_b;
    
    cnn.layers{1}.sum_spikes{1} = cnn.layers{1}.sum_spikes{1} + inp_image_r;
    cnn.layers{1}.sum_spikes{2} = cnn.layers{1}.sum_spikes{2} + inp_image_g;
    cnn.layers{1}.sum_spikes{3} = cnn.layers{1}.sum_spikes{3} + inp_image_b;
    
    inputmaps = 3;  %1
    
    for l = 2 : numel(cnn.layers)   %  for each layer
        if strcmp(cnn.layers{l}.type, 'c')
            % Convolution layer, output a map for each convolution
            for j = 1 : cnn.layers{l}.outputmaps
                % Sum up input maps
                z = zeros(size(cnn.layers{l - 1}.spikes{1}) - [cnn.layers{l}.kernelsize - 1 cnn.layers{l}.kernelsize - 1 0]);
                for i = 1 : inputmaps   %  for each input map
                    %  convolve with corresponding kernel and add to temp output map
                    z = z + convn(cnn.layers{l - 1}.spikes{i}, cnn.layers{l}.k{i}{j}, 'valid');
                end
                % Only allow non-refractory neurons to get input
                z(cnn.layers{l}.refrac_end{j} > t) = 0;
                % Add input
                cnn.layers{l}.mem{j} = cnn.layers{l}.mem{j} + z;
                % Check for spiking
                cnn.layers{l}.spikes{j} = cnn.layers{l}.mem{j} >= opts.Thresh(l);
                % Reset
                cnn.layers{l}.mem{j}(cnn.layers{l}.spikes{j}) = 0;
                % Ban updates until....
                cnn.layers{l}.refrac_end{j}(cnn.layers{l}.spikes{j}) = t + opts.t_ref;
                % Store result for analysis later
                cnn.layers{l}.sum_spikes{j} = cnn.layers{l}.sum_spikes{j} + cnn.layers{l}.spikes{j};
            end
            %  set number of input maps to this layers number of outputmaps
            inputmaps = cnn.layers{l}.outputmaps;
        elseif strcmp(cnn.layers{l}.type, 's')
            %  Subsample by averaging
            for j = 1 : inputmaps
                % Average input
                z = convn(cnn.layers{l - 1}.spikes{j}, ones(cnn.layers{l}.scale) / (cnn.layers{l}.scale ^ 2), 'valid');
                % Downsample
                z = z(1 : cnn.layers{l}.scale : end, 1 : cnn.layers{l}.scale : end, :);
                % Only allow non-refractory neurons to get input
                z(cnn.layers{l}.refrac_end{j} > t) = 0;
                % Add input
                cnn.layers{l}.mem{j} = cnn.layers{l}.mem{j} + z;
                % Check for spiking
                cnn.layers{l}.spikes{j} = cnn.layers{l}.mem{j} >= opts.Thresh(l);
                % Reset
                cnn.layers{l}.mem{j}(cnn.layers{l}.spikes{j}) = 0;
                % Ban updates until....
                cnn.layers{l}.refrac_end{j}(cnn.layers{l}.spikes{j}) = t + opts.t_ref;              
                % Store result for analysis later
                cnn.layers{l}.sum_spikes{j} = cnn.layers{l}.sum_spikes{j} + cnn.layers{l}.spikes{j};                
            end
        end
    end

    % Concatenate all end layer feature maps into vector
    cnn.fv1 = [];
    for j = 1 : numel(cnn.layers{end}.spikes)
        sa = size(cnn.layers{end}.spikes{j});
        cnn.fv1 = [cnn.fv1; reshape(cnn.layers{end}.spikes{j}, sa(1) * sa(2), sa(3))];
    end
    cnn.sum_fv1 = cnn.sum_fv1 + cnn.fv1;
    
    % Run the output layer neurons
    %   Add inputs multiplied by weight
    impulse_intermediate = cnn.ffW1' * cnn.fv1;
    %   Only add input from neurons past their refractory point
    impulse_intermediate(cnn.o1_refrac_end >= t) = 0;

    %   Add input to membrane potential
    cnn.o1_mem = cnn.o1_mem + impulse_intermediate;
    %   Check for spiking
    cnn.o1_spikes = cnn.o1_mem >= 1;
    %   Reset
    cnn.o1_mem(cnn.o1_spikes) = 0;
    %   Ban updates until....
    cnn.o1_refrac_end(cnn.o1_spikes) = t + opts.t_ref;
    %   Store result for analysis later
    cnn.o1_sum_spikes = cnn.o1_sum_spikes + cnn.o1_spikes;
    
     cnn.fv2 = [];
     cnn.fv2 = cnn.o1_spikes;
%     for j = 1 : numel(cnn.o1_spikes)
%         sa = size(cnn.o1_spikes(j));
%         cnn.fv2 = [cnn.fv2; reshape(cnn.o1_spikes(j), sa(1) , sa(2))];
%     end
    cnn.sum_fv2 = cnn.sum_fv2 + cnn.fv2;
    
    % Run the output layer neurons
    %   Add inputs multiplied by weight
    cnn.ffWb = cnn.ffW2';
    impulse = cnn.ffWb * cnn.fv2;
    %   Only add input from neurons past their refractory point
    impulse(cnn.o2_refrac_end >= t) = 0;

    %   Add input to membrane potential
    cnn.o2_mem = cnn.o2_mem + impulse;
    %   Check for spiking
    cnn.o2_spikes = cnn.o2_mem >= 1;
    %   Reset
    cnn.o2_mem(cnn.o2_spikes) = 0;
    %   Ban updates until....
    cnn.o2_refrac_end(cnn.o2_spikes) = t + opts.t_ref;
    %   Store result for analysis later
    cnn.o2_sum_spikes = cnn.o2_sum_spikes + cnn.o2_spikes;
    
    % Tell the user what's going on
    if(mod(round(t/opts.dt),round(opts.report_every/opts.dt)) == ...
            0 && (t/opts.dt > 0))
        [~, guess_idx] = max(cnn.o2_sum_spikes);
        acc = sum(guess_idx==ans_idx)/size(test_ymt, 2) * 100;
        fprintf('Time: %1.3fs | Accuracy: %2.2f%%.\n', t, acc);
        cnn.performance(end+1) = acc;
    else
        fprintf('.');            
    end
end