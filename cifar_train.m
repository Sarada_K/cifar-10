function [net, net_model,r,g,b,ty,Layer_array,Map_output,Map_input,cnn_mod]= cifar_train(net, rx,gx,bx, y, opts)
    m = size(rx, 3);
    numbatches = m / opts.batchsize;
    if rem(numbatches, 1) ~= 0
        error('numbatches not integer');
    end
    net.rL = [];
    r = {}; g = {};b = {};ty = {}; Layer_array = {}; Map_output = {}; Map_input = {};
    cnn_mod = {};
    length = 1;
    checker = 1;
    net_model = cell(25,1);
    for i = 1 : opts.numepochs
        disp(['epoch ' num2str(i) '/' num2str(opts.numepochs)]);
        tic;
        kk = randperm(m);
        for l = 1 : numbatches
            batch_rx = rx(:, :, kk((l - 1) * opts.batchsize + 1 : l * opts.batchsize));
            batch_gx = gx(:, :, kk((l - 1) * opts.batchsize + 1 : l * opts.batchsize));
            batch_bx = bx(:, :, kk((l - 1) * opts.batchsize + 1 : l * opts.batchsize));
            batch_y = y(:,    kk((l - 1) * opts.batchsize + 1 : l * opts.batchsize));
            
            for lay = 2 : numel(net.layers)   %  for each layer
                if strcmp(net.layers{lay}.type, 'c')
                    num_maps = net.layers{lay}.outputmaps;
                    used_maps = rand(num_maps,1) > opts.dropout;
                    net.layers{lay}.used_maps = used_maps;
                end
            end
            net = cnnff(net, batch_rx, batch_gx, batch_bx);
            [net,Layer,Map_o,Map_i, FLAG] = cifar_bp(net, batch_y);
            
            if(any(FLAG(:)>0)==1)
                r{checker} = batch_rx;
                g{checker} = batch_bx;
                b{checker} = batch_gx;
                ty{checker} = batch_y;
                Layer_array{checker} = Layer;
                Map_output{checker} = Map_o;
                Map_input{checker} = Map_i;
                cnn_mod{checker} = net;
                
                checker = checker + 1;
            end    
            net = cnnapplygrads(net, opts);
            if isempty(net.rL)
                net.rL(1) = net.L;
            end
            net.rL(end + 1) = 0.99 * net.rL(end) + 0.01 * net.L;
        end
        toc;
        [er, bad] = cifar_test(net, rx,gx,bx,y);
        display(er);
        net_model{length} = net;
        length  = length + 1;
    end
    
    for lay = 2 : numel(net.layers)   %  for each layer
        if strcmp(net.layers{lay}.type, 'c')
            num_maps = net.layers{lay}.outputmaps;
            used_maps = ones(num_maps,1);
            net.layers{lay}.used_maps = used_maps;
        end
    end
end
