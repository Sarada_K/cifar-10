function [er, bad] = cifar_test(net, rx,gx, bx, y)
    %  feedforward
    net = cnnff(net, rx, gx, bx);
    [~, h] = max(net.o);
    [~, a] = max(y);
    bad = find(h ~= a);

    er = numel(bad) / size(y, 2);
end